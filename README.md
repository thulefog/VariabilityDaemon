# Domain Driven Design - Bounded Context Example Structure

This repository contains a rough non working code skeleton of a Domain Driven Design Ports + Adapters application skeleton, extracted from a working application for illustration purposes. 

The working application from which this structure was extracted - queries selected time domain calculations to assess Heart Rate Variability. These are derived measurements from Apple Watch collected heart rate samples.


# Bounded Context

The key Bounded Context in this example vertical slice is *TimeDomainVariabilityContext* where code for a complete use case would reside.

### Adapters

This is where the code for the Input and Output Adapters resides.

* FileStore

This is example code for utilty functions to read/write files in iOS application areas such as the Coche. An example use case is diagnostis logging.

* HealthStore

These adapters are used as *code behind* the Query services to fetch data from the Health Store.

* SharedKernel

As an illustration, this provies a seed for a binding layer between Swift, Objective-C and C++.


### ApplicationServices

This area defines key interfaces related to Command and Query separation of concerns (CQRS).

The typical convention of Command/Write and Query/Read is deviated slightly in favor of a more generic approach around executable Commands. 

The __Queues__ area provides hooks for the Operation and Notification Queues.

### Ports

### Repositories

This code area is where the Type System or Domain Model resides. These type definitions are an important backbone of the Domain Driven Design approach.

### TypeSystem

The Repository layer realized a Data Provider to coordinate data queries and caching of query results from the Health Store A very rough local store implementation is in place to provide rudimentary data caching.

### Presentation

The objective is to gather the User Interface code to modularize to the extent possible. Effectively what resides here is View Controllers and either from scratch code or Storyboards.

### TestContext

Code area for unit tests.

## Background

I had experimented with the VIPER approach described on some blog posts and open sourced projects for iOS but this more closely follows some of the canoncial reference books and a cleaner approach to DDD with a high degree of flexibility.
