//
//  BridgedTemplate.mm
//
//  Created by John Matthew Weston in April 2017, reworked January 2018
//  Copyright © 2017 Aquavit Designs - Open Source. All rights reserved.
//

#include "BridgeTemplate.hpp"
#include "TimeDomainReductions.h"

#include <iterator>
#include <vector>
using namespace std;

struct PointerToImplementationInner {
//    <INNER_CLASS_TYPE_NAME> *_wrappedInnerInstancePointer;
};

@implementation PointerToImplementationOuter

- (id) init {
    if (self = [super init]) {
        _wrappedOuterInstancePointer = new PointerToImplementationInner();
//        _wrappedOuterInstancePointer->_wrappedInnerInstancePointer = new <INNER_CLASS_TYPE_NAME>();
        
        
    } return self;
}

- (void) dealloc {
    delete _wrappedOuterInstancePointer->_wrappedInnerInstancePointer;
    delete _wrappedOuterInstancePointer;
}
 
- (void) pass: (double[]) data andDataLength: (int) dataLength {
    

}


@end

