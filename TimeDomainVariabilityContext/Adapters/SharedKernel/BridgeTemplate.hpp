//
//  BridgedTemplate.hpp
//
//  Created by John Matthew Weston in April 2017, reworked January 2018
//  Copyright © 2017 Aquavit Designs - Open Source. All rights reserved.
//

#ifndef BridgedTemplate_hpp
#define BridgedTemplate_hpp

#import <Foundation/Foundation.h>

#include <stdio.h>

struct PointerToImplementationInner;

@interface PointerToImplementationOuter: NSObject {
    struct PointerToImplementationInner *_wrappedOuterInstancePointer;
}

// We still aren't allowed to include C++ here, so we make this a void* to hold the object
@property (nonatomic) void* _pointerToImplementationInnerBinding;

@property (strong, nonatomic) id _key;

- (id) init;
- (void) dealloc;

- (void) pass: (double[]) data;

@end

#endif /* BridgedTemplate_hpp */
