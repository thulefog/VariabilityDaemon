//
//  GenericDataProvider.swift
//
//  Created by John Matthew Weston in June 2017.
//
//  Copyright © 2017 John Matthew Weston. All rights reserved.
//
//  This code is licensed under the MIT License:
//
//  Permission is hereby granted, free of charge, to any person obtaining a
//  copy of this software and associated documentation files (the "Software"),
//  to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense,
//  and/or sell copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//  DEALINGS IN THE SOFTWARE.
//

import Foundation

// MARK: Data Provider based on Generics adhering to protocol defined below
extension Dictionary{
    func containsValue<T : Equatable>(value : T)->Bool{
        let contains = self.contains { (k, v) -> Bool in
            
            if let v = v as? T, v == value{
                return true
            }
            return false
        }
        return contains
    }
    func containsKey<T : Equatable>(key : T)->Bool{
        let contains = self.contains { (k, v) -> Bool in
            
            if let k = k as? T, k == key {
                return true
            }
            return false
        }
        return contains
    }}

public struct GenericDataProvider<Element>: IGenericDataProvider {
    
    var keyValueStore = [String: Element ]()
    
    public mutating func append(key: String, value: Element) {
        self.keyValueStore[key] = value 
    }
    
    public var count: Int {
        return keyValueStore.count
    }
    
    public subscript(key: String) -> Element {
        return keyValueStore[key]!
    }


    
    
    public init() { }
    
}
