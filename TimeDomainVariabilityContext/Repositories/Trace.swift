//
//  Trace
//  Measurements
//
//  Trace log implementation
//
//  Copyright © 2015-2017 John Matthew Weston. All rights reserved.
//
//  This code is licensed under the MIT License:
//
//  Permission is hereby granted, free of charge, to any person obtaining a
//  copy of this software and associated documentation files (the "Software"),
//  to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense,
//  and/or sell copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//  DEALINGS IN THE SOFTWARE.
//

import Foundation

func log(logMessage: String, functionName: String = #function ) {
    print("\(functionName): \(logMessage)")
}

func trace(diagnostic: String )
{
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
    let documentsDirectory = paths.object(at: 0) as! NSString
    let path = documentsDirectory.appendingPathComponent("TRACE.LOG")
    let fileManager = FileManager.default
    var err:NSError?
    
    if( fileManager.fileExists( atPath: path ) == true ) {

        print( "File exists at path \(path), appending." )
    }
    else {
        print( "File not found at path \(path) - creating." )
        fileManager.createFile(atPath: path, contents: nil, attributes: nil)
    }
    if let fileHandle = FileHandle(forWritingAtPath: path ) {
        fileHandle.seekToEndOfFile()
        fileHandle.write( diagnostic.data(using: String.Encoding.utf8, allowLossyConversion: false)!)
    }
    else {
        print("Error: opening FileHandle for path \(path) with error \(err)")
    }
    
}
