//
//  RequestHealthKitAuthorizationAsync.swift
//  TImeDomainReductions
//
//  Created by John Matthew Weston on 2/9/18.
//  Copyright © 2018 Aquavit Designs - Open Source. All rights reserved.
//

import Foundation

class RequestHealthKitAuthorizationAsync : AsynchronousOperationBase {
    
    override func main() {
        if self.isCancelled {
            state = .finished
        } else {
            state = .executing
            
            print( "RequestHealthKitAuthorizationAsync - START")
            
            HealthKitDataProvider.sharedInstance.requestHealthKitAuthorization()
        }
    }
}
